﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Validacao.Models
{
    public class Jogador
    {
        [HiddenInput(DisplayValue = false)]
        public int JogadorId { get; set; }
        
        [Required (ErrorMessage = "O nome é Obrigatorio")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O número do Jogador é obrigatorio!")]
        [Range(1,99,ErrorMessage = "O numero do Jogador deve ser maior que 0 e menor que 100")]
        public int ? Numero { get; set; }
        [Required(ErrorMessage = "A altura do Jogador é obrigatoria")]
        [Range(0,double.MaxValue,ErrorMessage = "Digite uma altura valida!")]
        public double ? Altura { get; set; }
    }
}