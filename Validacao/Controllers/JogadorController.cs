﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Validacao.Models;

namespace Validacao.Controllers
{
    public class JogadorController : Controller
    {
        //
        // GET: /Jogador/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Lista()
        {
            DbContextAp ctx = new DbContextAp();
            return View(ctx.Jogadores);
        }
        [HttpGet]
        public ActionResult Cadastra()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Cadastra(Jogador j)
        {
            //if (String.IsNullOrEmpty(j.Nome))
            //{
            //    ModelState.AddModelError("O nome", "O nome do Jogador é obrigatorio!");
            //}
            //if (j.Numero == null || j.Numero <=0 || j.Numero>=100)
            //{
            //    ModelState.AddModelError("Numero","O numero do jogador dever ser maior que 0 e menor que 100");
            //}
            //if (j.Altura == null || j.Altura == 0)
            //{
            //    ModelState.AddModelError("Altura","Digite uma altura valida!");
            //}
            if (ModelState.IsValid)
            {
                DbContextAp ctx = new DbContextAp();
                ctx.Jogadores.Add(j);
                ctx.SaveChanges();
                return RedirectToAction("Lista");
            }
            else
            {
                return View("Cadastra",j);
            }
            
        }
    }
}
